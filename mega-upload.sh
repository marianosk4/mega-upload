#!/bin/sh

command -v jq >/dev/null || { echo "Error: program jq not found." >&2; exit 1; }
command -v curl >/dev/null || { echo "Error: program curl not found." >&2; exit 1; }
command -v hxselect >/dev/null || { echo "Error: program hxselect not found." >&2; exit 1; }
command -v mega-cmd >/dev/null || { echo "Error: program mega-cmd not found." >&2; exit 1; }

mega-logout

random_anum() {
	    </dev/urandom tr -dc 'A-Za-z0-9' | head -c "$1"; echo
}

login="$(random_anum 16)"
domain='1secmail.com'
email="${login}@${domain}"
password="$(random_anum 16)"

echo "signup"
echo 'mega-signup  "${email}" "${password}" --name ="Tu Vieja" '
mega-signup  "${email}" "${password}" --name ="Tu Vieja" 


printf 'Registration request sent.\n'

echo "https://www.1secmail.com/api/v1/?action=getMessages&login=${login}&domain=${domain}"
printf 'Checking for registration email: '
while true; do
    sleep 2
    messages="$(curl -s "https://www.1secmail.com/api/v1/?action=getMessages&login=${login}&domain=${domain}")"
    if [[ "$(jq 'length' <<< "${messages}")" -ge 1 ]]; then
        printf 'found.\n'
        break;
    fi
    printf '.'
done

msg_id="$(jq '.[0].id' <<< "${messages}")"
msg_body="$(curl -s "https://www.1secmail.com/api/v1/?action=readMessage&login=${login}&domain=${domain}&id=${msg_id}" | jq -r '.body')"

reg_link="$(hxselect '#bottom-button::attr(href)' <<< "${msg_body}" | cut -d '"' -f 2)"

echo "reg link: $reg_link"

mega-confirm "${reg_link}" "${email}" "${password}"
printf 'Registered with email "%s" and password "%s".\n' "${email}" "${password}"

echo "login:"
mega-login "${email}" "${password}"
echo "uploading:"
mega-put "${1}"
echo "share: "
yes | mega-export "/${1}" -a
